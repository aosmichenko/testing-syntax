<?php

get_header(); ?>



    <div class="bb-archive container">
        <?php
            $getCategory = get_queried_object();
            $categoryName = $getCategory->name;
            $categoryID = $getCategory->term_id;

            if($categoryName): ?>
                <div class="bb-archive__top">
                    <h1><?php _e('Unsere ', ''); echo $categoryName; ?></h1>
                </div>
            <?php endif; ?>
        <div class="bb-archive__content">
            <div class="bb-archive__content-inner">

	            <?php

	            $args = array(
		            'cat' => $categoryID,
                    'posts_per_page' => 9,
	            );
	            $getPosts = new WP_Query($args);

	            if ( $getPosts->have_posts() ) : while ( $getPosts->have_posts() ) : $getPosts->the_post();

		            $postPermalink = get_the_permalink();
		            $postTitle = get_the_title();
		            $postThumbnail = get_the_post_thumbnail(); ?>

                    <a href="<?= $postPermalink; ?>" class="bb-archive__single" title="<?php _e('Beitrag ', ''); echo $postTitle; _e(' ansehen', ''); ?>">
			            <?php if ($postThumbnail) : ?>
                            <div class="bb-archive__image">
					            <?= $postThumbnail; ?>
                            </div>
			            <?php endif; ?>
                        <!-- TODO: @Andy ACF Feld integrieren -->
                        <div class="bb-archive__catchline">
                            Platz für mögliche Überschrift
                        </div>
			            <?php if($postTitle): ?>
                            <div class="bb-archive__headline">
					            <?= $postTitle; ?>
                            </div>
			            <?php endif; ?>
                        <?php
                        $arrowFile = get_template_directory_uri() . '/assets/icons/svg/icon_arrow_orange_background.svg';
                        $arrowSVG = bbInlineSVG($arrowFile);
                        if($arrowSVG): ?>
                            <div class="bb-archive__link">
                                <?= $arrowSVG; ?>
                            </div>
                        <?php endif; ?>
                    </a>

	            <?php endwhile;
		            // TODO: @Andy @Dali Grundgerüst für Fehlermeldung?
	            endif; wp_reset_postdata(); ?>

            </div>
	        <?php echo do_shortcode('[ajax_load_more pause="true" scroll="false" css_classes="bb-archive__content-inner" transition_container="false" container_type="div" post_type="post" posts_per_page="9" category="rezepte" offset="9" images_loaded="true" button_label="Mehr laden"]'); ?>
        </div>
    </div>



<?php get_footer(); ?>
